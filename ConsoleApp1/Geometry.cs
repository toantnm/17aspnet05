﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Point
    {
        int X;
        int Y;

        public Point()
        {
            X = 0;
            Y = 0;
        }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public void SetX(int x)
        {
            X = x;
        }

        public void SetY(int y)
        {
            Y = y;
        }

        public void SetPoint(int x, int y)
        {
            X = x;
            Y = y;
        }

        public double Distance(Point p)
        {
            return Math.Sqrt(Math.Pow(X * p.X, 2) + Math.Pow(Y * p.Y, 2));
        }
    }

    class Circle
    {
        public Point Center;
        public double Radius;

        public Circle() { }

        public Circle(Point center)
        {
            Center = center;
        }

        public Circle(Point center, double radius) : this(center)
        {
            Radius = radius;
        }

        public bool IsContainsPoint(Point point)
        {
            double distance = Center.Distance(point);
            if (distance > Radius) return false;
            return true;
        }
    }


}
