﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class PhanSo
    {
        public int TuSo { get; set; }
        public int MauSo { get; set; }

        public PhanSo()
        {
            TuSo = 0;
            MauSo = 1;
        }

        public PhanSo(int tuso)
        {
            TuSo = tuso;
            MauSo = 1;
        }

        public PhanSo(int tuso, int mauso) : this(tuso)
        {
            MauSo = mauso;
        }

        public void SetTuSo(int tuso, int mauso)
        {
            TuSo = tuso;
            MauSo = mauso;
        }

        public void Display()
        {
            int uc = Ucln(TuSo, MauSo);
            Console.WriteLine("{0}/{1}", TuSo/uc, MauSo/uc);
        }

        public PhanSo Add(PhanSo phanso)
        {
            if (MauSo == 0) throw new Exception("Không thể thực hiện phép tính");
            int mauchung = Bcnn(MauSo, phanso.MauSo);
            int tu = TuSo * (mauchung / MauSo) + phanso.TuSo * (mauchung / phanso.MauSo);
            return new PhanSo(tu, mauchung);
            
        }

        private int Bcnn(int a, int b)
        {
            return a * b / Ucln(a, b);
        }

        private int Ucln(int a, int b)
        {
            return (b == 0) ? a : Ucln(b, a % b);
        }
    }
}
