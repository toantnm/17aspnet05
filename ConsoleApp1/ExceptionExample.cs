﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class ExceptionExample
    {
        public static float Div(int a, int b)
        {
            try
            {
                return (float)a / b;
            }
            catch
            {
                throw new Exception("Chia cho 0");
            }
        }
    }
}
