﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ConsoleApp3.MyExtensions;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 12345;
            Console.WriteLine(a.Reverse());
            Console.WriteLine(a.MultiplyWith(2));

            var student = new { Id = 1, Name = "a" }; //đối tượng Anonymous

            List<int> numbers = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            //select * from numbers where col %2 = 0
            var even_nums = from x in numbers
                            where x % 2 == 0
                            select x;
            foreach (var item in even_nums)
            {
                Console.WriteLine(item);
            }

            List<Person> people = new List<Person>();
            people.Add(new Person()
            {
                Id = 1,
                Name = "abc",
                Address = "",
                Email = "",
                Age = 15
            });
            people.Add(new Person()
            {
                Id = 2,
                Name = "hohof",
                Address = "",
                Email = "",
                Age = 15
            });
            people.Add(new Person()
            {
                Id = 3,
                Name = "dfss",
                Address = "",
                Email = "",
                Age = 16
            });
            people.Add(new Person()
            {
                Id = 4,
                Name = "asda",
                Address = "",
                Email = "",
                Age = 17
            });
            people.Add(new Person()
            {
                Id = 5,
                Name = "asd",
                Address = "",
                Email = "",
                Age = 14
            });

            var subPerson = from p in people
                            where p.Age > 15 && p.Address == "" && Regex.IsMatch(p.Email, @"^.+@gmail.com$")
                            select new { MaSinhVien = p.Id, HoTen = p.Name };

            //foreach (var item in subPerson)
            //{
            //    Console.WriteLine("{0},{1}", item.MaSinhVien, item.HoTen);
            //}

            var subPerson2 = people
                .Where(p => p.Age > 15 && p.Address == "" && Regex.IsMatch(p.Email, @"^.+@gmail.com$"))
                .Select(x => new { x.Id, x.Name });

            subPerson.ToList().ForEach(x => Console.WriteLine("{0},{1}", x.MaSinhVien, x.HoTen));

            var products = new List<ProductInfo>();
            products.Add(new ProductInfo()
            {
                Name = "b",
                Description = "",
                NumberinStock = 11
            });
            products.Add(new ProductInfo()
            {
                Name = "c",
                Description = "",
                NumberinStock = 3
            });
            products.Add(new ProductInfo()
            {
                Name = "d",
                Description = "",
                NumberinStock = 5
            });
            products.Add(new ProductInfo()
            {
                Name = "e",
                Description = "",
                NumberinStock = 12
            });
            products.Add(new ProductInfo()
            {
                Name = "a",
                Description = "",
                NumberinStock = 14
            });

            //product có tồn kho >50 sản phẩm
            var p1 = from p in products
                     where p.NumberinStock > 50
                     select p;
            var p2 = products.Where(p => p.NumberinStock > 50);

            //product có tên chứa ký tự a hoặc tồn kho dưới 20
            var p3 = from p in products
                     where p.Name.Contains('a') && p.NumberinStock < 20
                     select p;
            var p4 = products.Where(p => p.Name.Contains('a') && p.NumberinStock < 20);

            //product có tên dài hơn 5 ký tự
            var p5 = from p in products
                     where p.Name.Length > 5
                     select p;
            var p6 = products.Where(p => p.Name.Length > 5);

            //product có tồn kho từ 40 - 60
            var p7 = from p in products
                     where p.NumberinStock <= 60 && p.NumberinStock >= 40
                     select p;
            var p8 = products.Where(p => p.NumberinStock <= 60 && p.NumberinStock >= 40);

            int total = products.Sum(x => x.NumberinStock);
            double everage = products.Average(x => x.NumberinStock);
            int count = products.Count(x => x.NumberinStock > 50);
            var first = products.FirstOrDefault(x=>x.Name.Contains("a"));
            var top3 = products.Where(x => x.NumberinStock > 50).Take(3);
            int pageNumber = 1;
            var page1 = products.OrderByDescending(x => x.Name)
                .Skip((pageNumber - 1) * 2)
                .Take(2);

            Console.ReadKey();
        }

        class Person
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Address { get; set; }
            public string Email { get; set; }
            public int Age { get; set; }
        }
    }
}
