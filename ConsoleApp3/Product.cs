﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    public class ProductInfo
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int NumberinStock { get; set; }

        public override string ToString()
        {
            return string.Format("Name={0}, Description={1}, Number In Stock={2}", Name, Description, NumberinStock);
        }
    }
}
