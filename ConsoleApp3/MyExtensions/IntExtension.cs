﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3.MyExtensions
{
    public static class IntExtension
    {
        public static string Reverse(this int number)
        {
            return number.ToString().Reverse().ToString();
        }

        public static int MultiplyWith(this int number, int i)
        {
            return number * i;
        }
    }
}
