﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Subject
    {
        public string Name { get; set; }
        public double Match { get; set; }

        public Subject() { }
        public Subject(string name, double match)
        {
            Name = name;
            Match = match;
        }
    }
}
