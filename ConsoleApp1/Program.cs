﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        public enum MethodResult
        {
            SUSSCESS = 200,
            FAIL
        }

        static void Main(string[] args)
        {
            //Bulk.CcApiClient bulk = new Bulk.CcApiClient();
            //Bulk.Result result = bulk.wsCpMt("", "", "", "", "", "", "", "", "", "");
            //result.result1 = 
            
            var str = "    Nguyen    Van   Vuong   ";
            //string s = string.Join(" ", str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
            //string s = TrimSpace(str).Trim();
            StringBuilder sb = new StringBuilder();
            sb.Append("xyz");
            sb.AppendFormat("abc {0}", "123");

            string s = sb.ToString();

            Console.WriteLine(s);

            int a = 1;
            int b = 2;
            Console.WriteLine(Add(ref a, ref b));
            Console.WriteLine("a={0}, b={1}", a, b);

            Console.WriteLine(Sum(1, 2, 3));

            WriteLog(msg: "Test", level: 2, user: "Guest");

            int? x = null;
            //Nullable<int> y = null;
            x.GetValueOrDefault(0);
            var y = x ?? 0;

            //Console.ReadKey();

            Student.Display("abc");

            Student sv1 = Student.Create();
            sv1.Name = "1";
            Student sv2 = Student.Create();
            sv2.Name = "2";

            Console.WriteLine(sv1.Name);

            Student student1 = new Student
            {
                SubjectA = new Subject("Toán", 10.0)
            };

            Student student2 = new Student
            {
                SubjectA = new Subject("Toán", 8.1)
            };

            Console.WriteLine(student1.CompareMatch(student2));

            //Phân số
            var phanso1 = new PhanSo(2, 9);
            var phanso2 = new PhanSo(3, 10);
            phanso1.Add(phanso2).Display();

            var p1 = new Point(2, 9);
            var p2 = new Point(3, 10);
            Console.WriteLine(p1.Distance(p2));

            Console.ReadKey();
        }

        static MethodResult IsSuccess(int level)
        {
            if (level < 100) return MethodResult.SUSSCESS;
            return MethodResult.FAIL;
        }
        static void WriteLog(string msg, string user = "Admin", int level = 1)
        {
            Console.WriteLine("Message: {0}, user:{1}, level{2}", msg, user, level);
        }
        static int Sum(params int[] numbers)
        {
            int total = 0;
            foreach (var num in numbers)
            {
                total += num;
            }
            return total;
        }

        static string TrimSpace(string s)
        {
            return Regex.Replace(s, @"\s+", " ");
        }

        static int Add(ref int a, ref int b)
        {
            a = 1000;
            b = 2000;
            return a + b;
        }

        static void Add(int a, int b, out int c)
        {
            c = a + b;
        }
    }

    
}
