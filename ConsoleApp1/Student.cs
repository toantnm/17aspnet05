﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    partial class Student
    {
        public const double PI = 3.14;

        public readonly string HashCode = "abc@123";//Cho phép thay đổi giá trị trong constructor
        //singleton: luôn gán cho 1 đối tượng
        private static Student instance;
        public static Student Create()
        {
            if(instance == null)
            {
                instance = new Student();
            }
            return instance;
        }

        public int Id {get;set ;}
        public string Name { get; set; }
        public Subject SubjectA { get; set; }

        public Student()
        { }

        public Student(int id)
        {
            Id = id;
        }

        public Student(int id, string name) : this(id)
        {
            Name = name;
            Init();
        }
        
        public static void Display(string name)
        {
            Console.WriteLine(name);
        }

        public int CompareMatch(Student std)
        {
            if (SubjectA.Match > std.SubjectA.Match) return 1;
            else if (SubjectA.Match == std.SubjectA.Match) return 0;
            return -1;
        }
    }
}
